use clap::{
    App,
    Arg
};
use std::{
    process,
    fs::File,
    io::prelude::*,
    fmt,
    mem::transmute
};
use goblin::{
    pe::{
        PE
    }
};

macro_rules! bail {
    () => (process::exit(1));
    ($($arg:tt)*) => ({
        eprintln!("{}", &format!("{}\n", format_args!($($arg)*)));
        process::exit(1);
    })
}

enum Subsystem {
    CUI,
    GUI,
    Other(String)
}

impl fmt::Display for Subsystem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let str_repr = match self {
            Subsystem::CUI => "Console User Interface",
            Subsystem::GUI => "Graphic User Interface",
            Subsystem::Other(repr) => repr
        };
        write!(f, "{}", str_repr)
    }
}

fn main() {
    let args = App::new("pe-binary-editor")
        .arg(
            Arg::with_name("operation")
            .index(1)
            /*.possible_value("deelevate")
            .possible_value("elevate")*/
            .possible_value("guify")
            .possible_value("clify")
            .help("Operation to perform on binary")
            .required(true)
        )
        .arg(
            Arg::with_name("filename")
            .index(2)
            .help("Path to the binary to modify")
            .required(true)
        )
        .arg(
            Arg::with_name("output")
            .index(3)
            .help("Output filename")
            .required(false)
        )
        .get_matches();
    let operation: String = args.value_of("operation").expect("Missing required argument").to_string();
    let input_file: String = args.value_of("filename").expect("Missing required argument").to_string();
    let mut output_file: String = String::from(&input_file);
    if args.is_present("output") {
        output_file = args.value_of("output").expect("Missing required argument").to_string();
    }
    println!("Performing operation '{}' on {}, putting result to {}", &operation, &input_file, &output_file);

    let binary = load_binary(&input_file);

    let binary = match &operation[..] {
        "guify" => operation_guify(binary),
        "clify" => operation_clify(binary),
        _ => bail!("Invalid operation: {}", &operation)
    };

    save_binary(&output_file, &binary);
}

fn load_binary(path: &str) -> Vec<u8> {
    let mut file = File::open(&path).expect("Unable to open input file");
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).expect("Unable to read input file");
    contents
}

fn save_binary(path: &str, bytes: &Vec<u8>) {
    let mut file = File::create(&path).expect("Unable to create output file");
    file.write(&bytes).expect("Unable to write to output file");
}

fn get_subsystem(bytes: &Vec<u8>) -> Subsystem {
    let pos = calculate_subsystem_position(bytes);
    let subsystem: u16 = (
        ((bytes[pos + 1] as u16) << 8) |
        ((bytes[pos + 0] as u16) << 0)
    ) as u16;
    match subsystem {
        2 => Subsystem::GUI,
        3 => Subsystem::CUI,
        _ => Subsystem::Other(
            (
                match subsystem {
                0 => "Unknown",
                1 => "Native",
                2 => "Graphic User Interface",
                3 => "Console User Interface",
                5 => "OS/2",
                7 => "Posix",
                8 => "Win9x",
                9 => "WinCE",
                10 => "EFI Application",
                11 => "EFI Boot Service Driver",
                12 => "EFI Runtime Driver",
                13 => "EFI ROM",
                14 => "X-Box",
                _ => "Invalid"
            }).to_string()
        )
    }
}

fn set_subsystem(bytes: Vec<u8>, subsystem: Subsystem) -> Vec<u8> {
    let pos = calculate_subsystem_position(&bytes);
    let subsystem: u16 = match subsystem {
        Subsystem::GUI => 2,
        Subsystem::CUI => 3,
        Subsystem::Other(subsystem_str) => { bail!("Attempted to set unsupported subsystem ({})", subsystem_str) }
    };
    let new_bytes: Vec<u8> = vec![
        ((subsystem >> 0) & 0xFF) as u8,
        ((subsystem >> 8) & 0xFF) as u8
    ];
    let mut bytes = bytes.clone();
    bytes[pos + 0] = new_bytes[0];
    bytes[pos + 1] = new_bytes[1];
    bytes
}

fn calculate_subsystem_position(bytes: &Vec<u8>) -> usize {
    let pe = PE::parse(&bytes).expect("Unable to parse input file as a PE binary");
    let header = pe.header;
    header.optional_header.expect("No optional header, cannot determine subsystem");

    let pe_signature_offset_location = 0x3c;
    let nt_header_offset: i32 = (
        ((bytes[pe_signature_offset_location + 3] as u32) << 24) |
        ((bytes[pe_signature_offset_location + 2] as u32) << 16) |
        ((bytes[pe_signature_offset_location + 1] as u32) << 8) |
        ((bytes[pe_signature_offset_location + 0] as u32) << 0)
    ) as i32;
    if nt_header_offset as usize > bytes.len() {
        bail!("Went past byte count, tried {:#x}, maximum is {:#x}", nt_header_offset, bytes.len());
    }

    // let's calculate our offset
    let mut current_position = nt_header_offset as usize; // seek on 2940
    current_position += 4; // u32 read on 2945
    // descend into COFF header reading here
    current_position += 2; // u16 read on 2743
    current_position += 2; // i16 read on 2744
    current_position += 4; // i32 read on 2745
    current_position += 4; // i32 read on 2746
    current_position += 4; // i32 read on 2747
    current_position += 2; // i16 read on 2748
    current_position += 2; // u16 read on 2749
    // step out of COFF header reading here
    // get bit count from peeking a u16 here
    let bit_count_magic: u16 = (
        ((bytes[current_position + 1] as u16) << 8) |
        ((bytes[current_position + 0] as u16) << 0)
    ) as u16; // read but don't advance current position, let reading standard fields take care of that
    let sniffed_64_bit = match bit_count_magic {
        0x010B => false,
        0x020B => true,
        _ => { bail!("Invalid bit magic, got {}, read from offset {}, assuming NT header at {}", bit_count_magic, current_position, nt_header_offset) }
    };
    if sniffed_64_bit != pe.is_64 {
        bail!("Sniffed different architectures, sniffed 64-bit: {}, true is 64-bit: {}", sniffed_64_bit, pe.is_64);
    }

    let standard_fields_offset_32_bit =  6 * 4 + 1 * 2 + 2 * 1;
    let standard_fields_offset_64_bit = 5 * 4 + 1 * 2 + 2 * 1;

    if sniffed_64_bit {
        current_position += standard_fields_offset_64_bit;
    } else {
        current_position += standard_fields_offset_32_bit;
    }

    // now before additional fields

    let nt_subsystem_offset_32_bit = 7 * 4 + 6 * 2;
    let nt_subsystem_offset_64_bit = 1 * 8 + 6 * 4 + 6 * 2;
    if sniffed_64_bit {
        current_position += nt_subsystem_offset_64_bit;
    } else {
        current_position += nt_subsystem_offset_32_bit;
    }

    current_position
}

fn operation_deelevate() {

}

fn operation_elevate() {

}

fn operation_guify(bytes: Vec<u8>) -> Vec<u8> {
    switch_subsystem(bytes, Subsystem::GUI)
}

fn operation_clify(bytes: Vec<u8>) -> Vec<u8> {
    switch_subsystem(bytes, Subsystem::CUI)
}

fn switch_subsystem(bytes: Vec<u8>, subsystem: Subsystem) -> Vec<u8> {
    let subsystem_before = get_subsystem(&bytes);
    println!("Subsystem before: {}", subsystem_before);
    println!("Targeting subsystem: {}", subsystem);
    let bytes = set_subsystem(bytes, subsystem);
    let subsystem_after = get_subsystem(&bytes);
    println!("Subsystem after: {}", subsystem_after);
    bytes
}